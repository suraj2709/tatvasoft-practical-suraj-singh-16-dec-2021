<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware("guest");
    }

    public function showLoginForm()
    {
        return view("login");
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            "email" => "required|email|exists:users,email",
            "password" => "required"
        ]);

        if(Auth::attempt(["email" => $request->email, "password" => $request->password])){
            if($request->redirect){
                return redirect($request->redirect);
            }
            return redirect()->route("home");
        }else{
            return redirect()->back()->with("error", "These credentials do not match our records.");
        }
    }
}
