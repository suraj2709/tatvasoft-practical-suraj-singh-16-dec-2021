@extends('layout')

@section('content')
@if(count($blogs) == 0)
<div class="card mt-4">
    <div class="card-body">
    No blogs found.
    </div>
</div>
@else 
<div class="card mt-4">
    <div class="card-body">
        @if(Session::has('message'))
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
        @endif
        @foreach($blogs as $blog)
        <div class="row mt-3">
            <div class="col-md-4">
                <img src="{{asset($blog->image)}}" class="image-responsive" style="width:300px;height:200px" alt="">
            </div>
            <div class="col-md-8">
                <div class="title">
                    <h2>{{$blog->title}}</h2>
                </div>
                <div class="description">
                    {{ strlen($blog->description) > 200 ? substr($blog->description, 0 , 200)."..." : $blog->description }}
                    <a href="{{route('blog.show', $blog->id)}}">Read Full article</a>
                </div>
                <br>
                <div class="option" style="display: flex;">
                    @auth
                    <a href="{{route('blog.edit', $blog->id)}}" class="btn btn-success">Edit</a>&nbsp;
                    <form action="{{route('blog.destroy', $blog->id)}}" method="POST">
                        @csrf
                        @method("DELETE")
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete?')">DELETE</button>
                    </form>
                    @endauth
                </div>
            </div>
        </div>
        @endforeach
        <br><br>
        {{$blogs->links()}}
    </div>
</div>
@endif
@endsection