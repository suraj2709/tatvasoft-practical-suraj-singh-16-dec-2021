@extends('layout')

@section('content')
<div class="card mt-4">
    <div class="card-body">
        @if(Session::has('message'))
        <div class="alert alert-success">
            {{Session::get('message')}}
        </div>
        @endif
        <div class="row mt-3">
            <div class="col-md-4">
                <img src="{{asset($blog->image)}}" class="image-responsive" style="width:300px;height:200px" alt="">
            </div>
            <div class="col-md-8">
                <div class="title">
                    <h2>{{$blog->title}}</h2>
                </div>
                <div class="description">
                    {{ $blog->description }}
                </div>
                <br>
                <div class="option" style="display: flex;">
                    @auth
                    <a href="{{route('blog.edit', $blog->id)}}" class="btn btn-success">Edit</a>&nbsp;
                    <form action="{{route('blog.destroy', $blog->id)}}" method="POST">
                        @csrf
                        @method("DELETE")
                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are you sure want to delete?')">DELETE</button>
                    </form>
                    @endauth
                </div>
            </div>
        </div>
    </div>
</div>
@endsection