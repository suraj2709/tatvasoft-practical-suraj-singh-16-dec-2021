<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Blog extends Model
{
    use HasFactory;

    protected $fillable = [
        "title", "description", "start_date", "end_date", "is_active", "image", "user_id"
    ];

    public function scopeRole(Builder $query)
    {
        if(auth()->user()->role != "admin"){
            $query->where("user_id", auth()->id());
        }
    }

    public function scopeActive(Builder $query)
    {
        $query->where("is_active", 1);
    }

    public function scopeNotExpired(Builder $query)
    {
        $query->whereDate("end_date", ">=", date("Y-m-d"));
    }
}
