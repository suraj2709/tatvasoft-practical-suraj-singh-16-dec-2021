@extends('layout')

@section('content')
<h2 class="text-center">Add New Blog</h2>
<div class="card mt-4">
    <div class="card-body">
        <form action="{{route('blog.update', $blog->id)}}" id="blog-form" enctype="multipart/form-data" method="POST">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="">Title</label>
                <input type="text" class="form-control" name="title" placeholder="Enter title" value="{{old('title', $blog->title)}}">
                @error('title')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="form-group mt-3">
                <label for="">Description</label>
                <textarea name="description" class="form-control" id="" placeholder="Enter Description">{{old('description', $blog->description)}}</textarea>
                @error('description')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="form-group  mt-3">
                <div class="row">
                    <div class="col-md-6">
                        <label for="">Start Date</label>
                        <input type="date" class="form-control" name="start_date" value="{{old('start_date', date('Y-m-d', strtotime($blog->start_date)))}}">
                        @error('start_date')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="col-md-6">
                        <label for="">End Date</label>
                        <input type="date" class="form-control" name="end_date" value="{{old('end_date', date('Y-m-d', strtotime($blog->end_date)))}}">
                        @error('end_date')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                </div>
            </div>

            <div class="form-group mt-3">
                <label for="">Image (Optional)</label>
                <input type="file" name="image" class="form-control">
                @error('image')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="form-group mt-3">
                <label for="">Status</label>
                <select name="is_active" id="" class="form-control">
                    <option value="1" {{$blog->is_active == 1 ? "selected" : ""}}>Active</option>
                    <option value="0" {{$blog->is_active != 1 ? "selected" : ""}}>Inactive</option>
                </select>
                @error('is_active')
                <span class="text-danger">{{$message}}</span>
                @enderror
            </div>

            <div class="form-group mt-3">
                <button type="button" class="btn btn-primary" id="btn-save-blog">Save Blog</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(){
        $("#btn-save-blog").click(function(){
            if($("input[name='title']").val() == ""){
                alert("Title field is required");
            }else if($("textarea[name='description']").val() == ""){
                alert("Description field is required");
            }else if($("input[name='start_date']").val() == ""){
                alert("Start date field is required");
            }else if($("input[name='end_date']").val() == ""){
                alert("End date field is required");
            }else if($("select[name='is_active']").val() == ""){
                alert("Status field is required");
            }else{
                $("#blog-form").submit();
            }
        })
    })
</script>
@endsection