<h2>To run this project, after pulling the project:</h2>
<ol>
    <li>Run <code>composer install</code></li>
    <li>Create a file name <code>.env</code></li>
    <li>Copy the contents of <code>.env.example</code></li>
    <li>Replace the DB credentials with your DB credentials</li>
    <li>Run command <code>php artisan migrate</code></li>
    <li>If you face any errors, then drop all the tables from database if exists, import the attached database with this repository.</li>
    <li>Run the command <code>php artisan storage:link</code></li>
    <li>Run command <code>php artisan serve</code></li>
</ol>