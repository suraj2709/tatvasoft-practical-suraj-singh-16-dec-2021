<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Register</title>
  </head>
  <body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center mt-4">
                <h2>Register</h2>
            </div>
            <div class="row">
                <div class="col-md-8 offset-2">
                    <form id="register-form" action="" method="post" enctype="multipart/form-data">
                        @csrf
                        @if(Session::has('error'))
                        <div class="alert alert-danger">
                            {{Session::get('error')}}
                        </div>
                        @endif
                        <div class="form-group">
                            <label for="">First Name</label>
                            <input type="text" class="form-control" name="first_name" value="{{old('first_name')}}">
                            @error("first_name")
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group mt-2">
                            <label for="">Last Name</label>
                            <input type="text" class="form-control" name="last_name"  value="{{old('last_name')}}">
                            @error("last_name")
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group mt-2">
                            <label for="">Email</label>
                            <input type="text" class="form-control" name="email" value="{{old('email')}}">
                            @error("email")
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group mt-2">
                            <label for="">Password</label>
                            <input type="password" class="form-control" name="password">
                            @error("password")
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group mt-2">
                            <label for="">Dob</label>
                            <input type="date" class="form-control" name="dob" value="{{old('dob')}}">
                            @error("dob")
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group mt-2">
                            <label for="">Image</label>
                            <input type="file" class="form-control" name="profile">
                            @error("profile")
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group mt-2">
                            <label for="">Role</label>
                            <select name="role" id="" class="form-control">
                                <option value="user" {{old("role") != "admin" ? "selected" : ""}}>User</option>
                                <option value="admin" {{old("role") == "admin" ? "selected" : ""}}>Admin</option>
                            </select>
                            @error("role")
                            <span class="text-danger">{{$message}}</span>
                            @enderror
                        </div>

                        <div class="form-group mt-2">
                            <button type="button" id="btn-register" class="btn btn-primary">
                                Register
                            </button>
                        </div>
                    </form>
                    <span>Already have an account. <a href="{{route('login-form')}}">Login</a></span>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script>
        $(document).ready(function(){
            $("#btn-register").click(function(){
                if($("input[name='first_name']").val() == ""){
                    alert("First name field is required");
                }else if($("input[name='last_name']").val() == ""){
                    alert("Last name field is required");
                }else if($("input[name='email']").val() == ""){
                    alert("Email field is required");
                }else if($("input[name='password']").val() == ""){
                    alert("Password field is required");
                }else if($("input[name='dob']").val() == ""){
                    alert("DOB field is required");
                }else if($("select[name='role']").val() == ""){
                    alert("Role field is required");
                }else if($('input[name="profile"]').get(0).files.length === 0){
                    alert("Profile Image is required");
                }else{
                    $("#register-form").submit();
                }
            })
        })
    </script>
  </body>
</html>