<?php

use App\Http\Controllers\BlogController;
use App\Http\Controllers\HomeConrtoller;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\RegisterConrtoller;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route("blog.index");
});
Route::get("login", [LoginController::class, "showLoginForm"])->name('login-form');
Route::post("login", [LoginController::class, "login"])->name('login');
Route::get("register", [RegisterConrtoller::class, "showRegisterForm"])->name('register-form');
Route::post("register", [RegisterConrtoller::class, "register"])->name('register');
Route::post('logout', [HomeConrtoller::class, 'logout'])->name("logout");
Route::get("home", function() {
    return redirect()->route("blog.index");
})->name("home");
Route::resource("blog", BlogController::class);