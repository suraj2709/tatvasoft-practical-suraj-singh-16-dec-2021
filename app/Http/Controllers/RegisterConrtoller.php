<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class RegisterConrtoller extends Controller
{
    public function __construct()
    {
        $this->middleware("guest");
    }

    public function showRegisterForm()
    {
        return view("register");
    }

    public function register(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            "first_name" => "required|string|max:255",
            "last_name" => "required|string|max:255",
            "email" => "required|email|unique:users,email",
            "password" => "required|min:8|max:72",
            "dob" => "required|date|before:today",
            "profile" => "required|image|max:2048",
            "role" => "required|in:admin,user"
        ]);

        $user = User::create([
            "first_name" => $request->first_name,
            "last_name" => $request->last_name,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            "dob" => date("Y-m-d", strtotime($request->dob)),
            "image" => "/storage/".$request->file("profile")->store("users_profile"),
            "role" => $request->role
        ]);

        Auth::login($user);

        return redirect()->route("home");
    }
}
