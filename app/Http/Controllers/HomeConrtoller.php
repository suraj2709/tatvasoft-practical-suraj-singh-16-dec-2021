<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeConrtoller extends Controller
{
    public function __construct()
    {
        $this->middleware("auth");
    }

    public function home()
    {
        return view("home");
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route("login-form");
    }
}
