<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::latest()->active()->notExpired();

        if(auth()->user()){
            $blogs->role();
        }

        $blogs = $blogs->paginate(10);

        return view("blog.index", compact('blogs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(!auth()->check()){
            return redirect()->route("login-form", ["redirect" => route("blog.create")]);
        }
        return view("blog.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(!auth()->check()){
            return redirect()->route("login-form", ["redirect" => route("blog.create")]);
        }

        $this->validate($request, [
            "title" => "required|max:255",
            "description" => "required",
            "start_date" => "required|date",
            "end_date" => "required|date|after_or_equal:start_date",
            "image" => "required|image|max:2048",
            "is_active" => "required|in:0,1"
        ]);

        Blog::create([
            "title" => $request->title,
            "description" => $request->description,
            "start_date" => date('Y-m-d', strtotime($request->start_date)),
            "end_date" => date('Y-m-d', strtotime($request->end_date)),
            "image" => "/storage/".$request->file('image')->store("blogs"),
            "user_id" => auth()->id(),
            "is_active" => $request->is_active,
        ]);

        return redirect()->route("blog.index")->with('message', "Blog created successfully.");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $blog = Blog::where("id", $id);

        if(auth()->check()){
            $blog->role();
        }

        $blog = $blog->firstOrFail();

        return view("blog.show", compact("blog"));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!auth()->check()){
            return redirect()->route("login-form", ["redirect" => route("blog.edit")]);
        }

        $blog = Blog::role()->where(["id" => $id])->firstOrFail();
        return view("blog.edit",  compact('blog'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(!auth()->check()){
            return redirect()->route("login-form", ["redirect" => route("blog.edit")]);
        }

        $blog = Blog::role()->where(["id" => $id])->firstOrFail();

        $this->validate($request, [
            "title" => "required|max:255",
            "description" => "required",
            "start_date" => "required|date",
            "end_date" => "required|date|after_or_equal:start_date",
            "image" => "nullable|image|max:2048",
            "is_active" => "required|in:0,1"
        ]);

        $data = [
            "title" => $request->title,
            "description" => $request->description,
            "start_date" => date('Y-m-d', strtotime($request->start_date)),
            "end_date" => date('Y-m-d', strtotime($request->end_date)),
            "is_active" => $request->is_active
        ];

        if($request->hasFile('image')){
            $data["image"] = "/storage/".$request->file('image')->store("blogs");
        }

        $blog->update($data);

        return redirect()->route("blog.index")->with('message', "Blog updated successfully.");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::role()->where(["id" => $id])->firstOrFail();
        $blog->delete();

        return redirect()->route("blog.index")->with('message', "Blog deleted successfully.");
    }
}
